package fe.timedbiometrics

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.preference.PreferenceManager
import com.google.accompanist.pager.*
import com.google.android.material.composethemeadapter.MdcTheme
import fe.timedbiometrics.keyguard.getFlagState
import fe.timedbiometrics.keyguard.updateKeyguardFlag
import fe.timedbiometrics.notification.createNotificationChannel
import fe.timedbiometrics.ui.DeviceAdminCard
import fe.timedbiometrics.ui.PrivilegeWarnCard
import fe.timedbiometrics.viewmodel.PermissionViewModel


class ControlYourDeviceActivity : AppCompatActivity() {
    private val dpm: Lazy<DevicePolicyManager> = lazy {
        getSystemService(DevicePolicyManager::class.java)
    }

    private var deviceOwnerComponent: Lazy<ComponentName> = lazy {
        ControlDeviceAdminReceiver().getWho(this)
    }

    fun isProfileOwner(): Boolean {
        return this.dpm.value.isProfileOwnerApp(this.applicationContext.packageName)
    }

    fun isDeviceOwner(): Boolean {
        return this.dpm.value.isDeviceOwnerApp(this.applicationContext.packageName)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getStrongAuthTimeout(): Long {
        return this.dpm.value.getRequiredStrongAuthTimeout(this.deviceOwnerComponent.value)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setStrongAuthTimeout(timeoutMs: Long) {
        this.dpm.value.setRequiredStrongAuthTimeout(this.deviceOwnerComponent.value, timeoutMs)
    }

    fun updateKeyguardFlag(flag: Int, state: Boolean) {
        this.dpm.value.updateKeyguardFlag(this.deviceOwnerComponent.value, flag, state)
    }

    fun isFlagDisabled(flag: Int): Boolean {
        return this.dpm.value.getFlagState(this.deviceOwnerComponent.value, flag)
    }

    private val permissionViewModel: PermissionViewModel by viewModels()

    @ExperimentalPagerApi
    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val deviceOwnerCallback =
            registerForActivityResult(AddDeviceAdmin(deviceOwnerComponent.value)) { success ->
                permissionViewModel.onDeviceAdminActiveChange(success)

                if (!success) {
                    Toast.makeText(
                        this,
                        getString(R.string.admin_permission_required),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        setContentView(R.layout.layout)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings_container, PreferenceFragment())
            .commit()

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        permissionViewModel.onDeviceAdminActiveChange(this.dpm.value.isAdminActive(this.deviceOwnerComponent.value))
        permissionViewModel.onDeviceProfileOwnerChange(
            (this.isDeviceOwner() && this.isProfileOwner()) ||
                    preferences.getBoolean(PRIVILEGE_WARN_HIDDEN, false)
        )

        val layoutStatus = findViewById<ComposeView>(R.id.layout_status)
        layoutStatus.setContent {
            MdcTheme {
                Column {
                    DeviceAdminCard(promptDeviceAdmin = {
                        deviceOwnerCallback.launch(Unit)
                    }, permissionViewModel)

                    PrivilegeWarnCard(learnFixClick = {
                        //TODO: implement open browser
                    }, hideClick = {
                        preferences.edit().apply {
                            this.putBoolean(PRIVILEGE_WARN_HIDDEN, true)
                        }.apply()
                    }, permissionViewModel)
                }
            }
        }

        createNotificationChannel(
            this,
            NOTIFICATION_CHANNEL,
            R.string.biometrics_disabled_name,
            R.string.biometrics_disabled_desc
        )

//        this.dpm.value.setMaximumFailedPasswordsForWipe()

        supportActionBar?.setDisplayHomeAsUpEnabled(false)
//
//        if (this.isProfileOwner() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            this.dpm.value.setResetPasswordToken(
//                this.deviceOwnerComponent.value,
//                "PU23A6FUDPQGFS6AFBDTY48T8D9883Z4".toByteArray()
//            )
//        }
        //

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.d(
                "Activity",
                "Can provision ${dpm.value.isProvisioningAllowed(DevicePolicyManager.ACTION_PROVISION_MANAGED_PROFILE)}"
            )
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//
//
//            dpm.value.setDelegatedScopes(deviceOwnerComponent.value, "net.typeblog.shelter", listOf(
//                DevicePolicyManager.DELEGATION_APP_RESTRICTIONS
//            ))
//        }
        //        dpm.value.

//        D/Shelter app: net.typeblog.shelter
//        D/Shelter app: net.typeblog.shelter.receivers.ShelterDeviceAdminReceiver

//        val shelterComponentName = ComponentName("net.typeblog.shelter", "net.typeblog.shelter.receivers.ShelterDeviceAdminReceiver")
//
//        val intent = Intent(DevicePolicyManager.ACTION_PROVISION_MANAGED_PROFILE)
//        intent.setPackage("net.typeblog.shelter")
//        intent.putExtra(DevicePolicyManager.EXTRA_PROVISIONING_SKIP_ENCRYPTION, true)
//        intent.putExtra(DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME, shelterComponentName)
//
//        startActivity(intent)
//        dpm.value.setMaximumFailedPasswordsForWipe(deviceOwnerComponent.value, 5)
    }

    companion object {
        private const val ADMIN_ACTIVATION = 1
        const val NOTIFICATION_CHANNEL = "notiChannel"

        const val PRIVILEGE_WARN_HIDDEN = "pref_privilege_warn_hidden"

        val HK_GROTESK_FONT = FontFamily(
            Font(R.font.hkgrotesk)
        )
    }
}