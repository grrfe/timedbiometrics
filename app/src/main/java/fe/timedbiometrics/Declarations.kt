package fe.timedbiometrics

inline fun <T> Function0<T>.chain(crossinline func: () -> Unit): () -> Unit {
    return {
        this.invoke()
        func.invoke()
    }
}

fun <T> Set<T>.get(idx: Int): T? {
    this.forEachIndexed { index, t ->
        if(index == idx) {
            return t
        }
    }

    return null
}