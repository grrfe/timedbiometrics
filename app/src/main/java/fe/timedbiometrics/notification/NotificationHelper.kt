package fe.timedbiometrics.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build

fun createNotificationChannel(
    context: Context,
    name: String,
    channelName: Int,
    channelDesc: Int
) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        context.getSystemService(NotificationManager::class.java).createNotificationChannel(
            NotificationChannel(
                name, context.getString(channelName),
                NotificationManager.IMPORTANCE_LOW
            ).apply {
                description = context.getString(channelDesc)
            }
        )
    }
}