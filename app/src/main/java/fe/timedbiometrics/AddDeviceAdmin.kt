package fe.timedbiometrics

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import androidx.appcompat.app.AppCompatActivity

class AddDeviceAdmin(private val deviceOwnerComponent: ComponentName) :
    ActivityResultContract<Unit, Boolean>() {

    override fun createIntent(context: Context, input: Unit): Intent {
        return Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN).apply {
            this.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceOwnerComponent)
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Boolean {
        return resultCode == AppCompatActivity.RESULT_OK
    }
}