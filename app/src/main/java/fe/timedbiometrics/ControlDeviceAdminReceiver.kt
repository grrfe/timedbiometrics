package fe.timedbiometrics

import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.UserHandle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.preference.PreferenceManager

class ControlDeviceAdminReceiver : DeviceAdminReceiver() {
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onPasswordFailed(ctxt: Context, intent: Intent) {
        handle(ctxt)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onPasswordFailed(context: Context, intent: Intent, user: UserHandle) {
        handle(context)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun handle(context: Context) {
        Log.d("LoginReceiver", "Handling fail..")

        val devicePolicyManager = context.getSystemService(DevicePolicyManager::class.java)
        val deviceOwnerComponent = ControlDeviceAdminReceiver().getWho(context)

        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val enabled = preferences.getBoolean(
            context.getString(R.string.pref_reboot_after_n_failed_password_attempts_enabled),
            false
        )
        val attempts = preferences.getString(
            context.getString(R.string.pref_reboot_after_n_failed_password_attempts),
            "-1"
        )!!.toInt()

        Log.d(
            "LoginReceiver",
            "Enabled: $enabled, Allowed attempts $attempts, current failed: ${devicePolicyManager.currentFailedPasswordAttempts}"
        )

        if (enabled && devicePolicyManager.currentFailedPasswordAttempts >= attempts) {
//            devicePolicyManager.
            //TODO: maybe handle ongoing call exception
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//                devicePolicyManager.logoutUser(deviceOwnerComponent)
//            }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                devicePolicyManager.getParentProfileInstance(deviceOwnerComponent).lockNow()
//                devicePolicyManager.lockNow(DevicePolicyManager.FLAG_EVICT_CREDENTIAL_ENCRYPTION_KEY)
//            }
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                devicePolicyManager.lockNow(DevicePolicyManager.FLAG_EVICT_CREDENTIAL_ENCRYPTION_KEY)
//            }

//            devicePolicyManager.reb
//            devicePolicyManager.wipeData(0)

            devicePolicyManager.reboot(deviceOwnerComponent)
        }
    }
}