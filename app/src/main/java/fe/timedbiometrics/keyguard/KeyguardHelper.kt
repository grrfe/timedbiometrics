package fe.timedbiometrics.keyguard

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.util.Log

object KeyguardHelper {
    fun isDisabled(disabledFeatures: Int, flag: Int): Boolean {
        return (disabledFeatures and flag) != 0
    }

    fun setFlagState(disabledFeatures: Int, flag: Int, state: Boolean): Int {
        return if (state) (disabledFeatures or flag) else (disabledFeatures and flag.inv())
    }
}

fun DevicePolicyManager.getFlagState(deviceOwnerComponent: ComponentName, flag: Int): Boolean {
    return KeyguardHelper.isDisabled(this.getKeyguardDisabledFeatures(deviceOwnerComponent), flag)
}

fun DevicePolicyManager.updateKeyguardFlag(
    deviceOwnerComponent: ComponentName,
    flag: Int,
    state: Boolean
) {
    this.setDisabledFeatures(
        deviceOwnerComponent,
        KeyguardHelper.setFlagState(this.getDisabledFeatures(deviceOwnerComponent), flag, state)
    )
}

private fun DevicePolicyManager.setDisabledFeatures(
    deviceOwnerComponent: ComponentName, disabledFeatures: Int
) {
    Log.d("SetDisabled", "$disabledFeatures")
    this.setKeyguardDisabledFeatures(deviceOwnerComponent, disabledFeatures)
}

fun DevicePolicyManager.getDisabledFeatures(deviceOwnerComponent: ComponentName): Int {
    return this.getKeyguardDisabledFeatures(deviceOwnerComponent).also {
        Log.d("GetDisabled", "$it")
    }
}