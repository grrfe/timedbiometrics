package fe.timedbiometrics.time

import org.joda.time.DateTime

interface JobTime {
    fun isValid(now: DateTime): Boolean
    fun getRunAt(): DateTime
    fun getState(): Boolean
}

class InRangeTime(
    private val after: DateTime,
    private val before: DateTime,
    private val state: Boolean
) : JobTime {
    override fun isValid(now: DateTime): Boolean {
        return now.isAfter(after) && now.isBefore(before)
    }

    override fun getRunAt() = before

    override fun getState() = state
}

class BeforeBothTime(
    private val disableAt: DateTime,
    private val reEnableAt: DateTime
) : JobTime {
    private val run = if (disableAt.isBefore(reEnableAt)) disableAt else reEnableAt

    override fun isValid(now: DateTime): Boolean {
        return now.isBefore(disableAt) && now.isBefore(reEnableAt)
    }

    override fun getRunAt(): DateTime {
        return run
    }

    override fun getState(): Boolean {
        return run == disableAt
    }
}

class AfterBothTime(
    private val disableAt: DateTime,
    private val reEnableAt: DateTime,
    private val disableAtTomorrow: DateTime,
    reEnableAtTomorrow: DateTime
) : JobTime {
    private val run =
        if (disableAt.isBefore(reEnableAt)) disableAtTomorrow else reEnableAtTomorrow

    override fun isValid(now: DateTime): Boolean {
        return now.isAfter(disableAt) && now.isAfter(reEnableAt)
    }

    override fun getRunAt(): DateTime {
        return run
    }

    override fun getState(): Boolean {
        return run == disableAtTomorrow
    }
}