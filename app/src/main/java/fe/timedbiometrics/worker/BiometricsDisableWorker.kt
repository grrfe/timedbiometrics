package fe.timedbiometrics.worker

import android.app.KeyguardManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import androidx.work.*
import fe.timedbiometrics.ControlDeviceAdminReceiver
import fe.timedbiometrics.ControlYourDeviceActivity
import fe.timedbiometrics.PreferenceFragment
import fe.timedbiometrics.R
import fe.timedbiometrics.keyguard.getFlagState
import fe.timedbiometrics.keyguard.updateKeyguardFlag
import org.joda.time.DateTime
import org.joda.time.Duration
import org.joda.time.LocalTime
import java.util.concurrent.TimeUnit

class BiometricsDisableWorker(private val appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {
    override fun doWork(): Result {
        //true -> disable biometrics, false -> enable biometrics
        val currentTask = inputData.getBoolean(WORKER_SET_BIOMETRIC_STATE, true)
        val preferences = PreferenceManager.getDefaultSharedPreferences(appContext)

        val devicePolicyManager = appContext.getSystemService(DevicePolicyManager::class.java)
        val notificationManager = appContext.getSystemService(NotificationManager::class.java)
        val keyGuardManager = appContext.getSystemService(KeyguardManager::class.java)

        val deviceOwnerComponent = ControlDeviceAdminReceiver().getWho(appContext)
        val currentDisabled = devicePolicyManager.getKeyguardDisabledFeatures(deviceOwnerComponent)

        if (devicePolicyManager.getFlagState(
                deviceOwnerComponent,
                PreferenceFragment.DISABLE_BIOMETRICS_FLAG
            ) != currentTask
        ) {
            if (!currentTask && preferences.getBoolean(appContext.getString(R.string.pref_interval_enabled)) && keyGuardManager.isDeviceLocked) {
                devicePolicyManager.lockNow()
            }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                devicePolicyManager.resetPasswordWithToken(
//                    deviceOwnerComponent,
//                    "hans123",
//                    "PU23A6FUDPQGFS6AFBDTY48T8D9883Z4".toByteArray(),
//                    0
//                )
//            }

            Log.d("Worker", "Current task = $currentTask")
            try {
                devicePolicyManager.updateKeyguardFlag(
                    deviceOwnerComponent,
                    PreferenceFragment.DISABLE_BIOMETRICS_FLAG,
                    currentTask
                )

                if (preferences.getBoolean(appContext.getString(R.string.pref_interval_change_notification))) {
                    notificationManager.cancelAll()
                    sendNotification(currentTask)
                }
            } catch (s: SecurityException) {
                s.printStackTrace()
                return Result.failure()
            }
        } else {
            Log.d(
                "Worker",
                "Skipping work, currentDisabled=$currentDisabled, currentTask=$currentTask, no action needed.."
            )
        }

        //if currentTask = true => disable, next task = reEnable/disable_end
        val preferenceKey =
            if (currentTask) R.string.pref_disable_end else R.string.pref_disable_start

        val minutesFromMidnight = preferences.getInt(appContext.getString(preferenceKey))
        val job = createJob(minutesFromMidnight, !currentTask)

        Log.d(
            "Worker",
            "Enqueuing new job for ($minutesFromMidnight) ${minutesFromMidnight / 60}:${minutesFromMidnight % 60}, state=${!currentTask}"
        )

        WorkManager.getInstance(appContext).enqueue(job)
        return Result.success()
    }

    private fun SharedPreferences.getBoolean(key: String) = this.getBoolean(key, false)

    private fun SharedPreferences.getInt(key: String) = this.getInt(key, 0)

    private fun sendNotification(state: Boolean) {
        val notification = NotificationCompat.Builder(
            appContext,
            ControlYourDeviceActivity.NOTIFICATION_CHANNEL
        ).apply {
            setSmallIcon(R.drawable.ic_baseline_fingerprint_24)
            setContentTitle(appContext.getString(R.string.biometric_state_changed))
            setContentText(
                appContext.getString(
                    R.string.biometric_new_state,
                    appContext.getString(if (state) R.string.disabled else R.string.enabled)
                )
            )
            priority = NotificationCompat.PRIORITY_DEFAULT
            setContentIntent(
                PendingIntent.getActivity(
                    appContext,
                    0,
                    Intent(appContext, ControlYourDeviceActivity::class.java),
                    0
                )
            )
            setAutoCancel(true)
        }.build()

        with(NotificationManagerCompat.from(appContext)) {
            notify(BIOMETRIC_WORKER_NOTIFICATION_ID, notification)
        }
    }

    companion object {
        private const val WORKER_SET_BIOMETRIC_STATE = "workerDisableState"
        const val WORKER_TAG = "BiometricsDisableTag"

        const val BIOMETRIC_WORKER_NOTIFICATION_ID = 133788

        fun createJob(minutesFromMidnight: Int, disableState: Boolean): OneTimeWorkRequest {
            Log.d("CreateJob", "For minutes from midnight $minutesFromMidnight")
            return createJob(minutesFromMidnight / 60 to minutesFromMidnight % 60, disableState)
        }

        fun createJob(dateTime: DateTime, disableState: Boolean): OneTimeWorkRequest {
            return createJob(Duration(DateTime.now(), dateTime).standardMinutes, disableState)
        }

        private fun createJob(
            hourMinute: Pair<Int, Int>,
            disableState: Boolean
        ): OneTimeWorkRequest {
            Log.d("CreateJob", "For pair $hourMinute")
            val runAt = LocalTime(hourMinute.first, hourMinute.second).toDateTimeToday()
            val now = DateTime.now()
            val delay = Duration(
                now,
                if (now.isBefore(runAt)) runAt else runAt.plusDays(1)
            ).standardMinutes

            return createJob(delay, disableState)
        }

        private fun createJob(minuteDelay: Long, disableState: Boolean): OneTimeWorkRequest {
            Log.d("Setup", "Delaying for $minuteDelay minutes, disableState=$disableState")

            return OneTimeWorkRequestBuilder<BiometricsDisableWorker>()
                .setInitialDelay(minuteDelay, TimeUnit.MINUTES)
                .addTag(WORKER_TAG)
                .setInputData(workDataOf(WORKER_SET_BIOMETRIC_STATE to disableState))
                .build()
        }
    }
}