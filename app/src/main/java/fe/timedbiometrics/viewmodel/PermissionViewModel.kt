package fe.timedbiometrics.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PermissionViewModel : ViewModel() {
    private val _deviceAdminActive = MutableLiveData(false)
    val deviceAdminActive: LiveData<Boolean> = _deviceAdminActive

    private val _deviceProfileOwner = MutableLiveData(false)
    val deviceProfileOwner: LiveData<Boolean> = _deviceProfileOwner

    fun onDeviceAdminActiveChange(new: Boolean) {
        _deviceAdminActive.value = new
    }

    fun onDeviceProfileOwnerChange(new: Boolean){
        _deviceProfileOwner.value = new
    }
}