package fe.timedbiometrics

import android.app.admin.DevicePolicyManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.format.DateFormat
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.preference.*
import androidx.work.WorkManager
import black.old.spacedrepetitionowl.TimepickerPreference
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import fe.timedbiometrics.time.AfterBothTime
import fe.timedbiometrics.time.BeforeBothTime
import fe.timedbiometrics.time.InRangeTime
import fe.timedbiometrics.worker.BiometricsDisableWorker
import mobi.upod.timedurationpicker.TimeDurationPicker
import mobi.upod.timedurationpicker.TimeDurationPickerDialog
import org.joda.time.DateTime
import java.util.concurrent.TimeUnit
import kotlin.time.milliseconds
import androidx.annotation.NonNull
import androidx.preference.EditTextPreference.OnBindEditTextListener


class PreferenceFragment : PreferenceFragmentCompat() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //for some reason, reading the default values from the preferences xml is not possible
        // (or trivial I guess, (or I'm just very retarded)), so we just set our defaults as sharedPrefs; hacky, but works
        val preferences = PreferenceManager.getDefaultSharedPreferences(this.context)

        preferences.edit().apply {
            mapOf(
                getString(R.string.pref_disable_start) to DEFAULT_DISABLE_MIN,
                getString(R.string.pref_disable_end) to DEFAULT_RE_ENABLE_MIN
            ).forEach { (pref, default) ->
                if (!preferences.contains(pref)) {
                    this.putInt(pref, default)
                }
            }
        }.apply()
    }

    private lateinit var controlYourDeviceActivity: ControlYourDeviceActivity

    private lateinit var disableBiometricsSwitch: SwitchPreferenceCompat
    private lateinit var intervalEnabledSwitch: SwitchPreferenceCompat

    private lateinit var disableBiometricsAtTime: TimepickerPreference
    private lateinit var reEnableBiometricsAtTime: TimepickerPreference

    private lateinit var applySettingsPref: Preference

    private lateinit var rebootAfterFailedAttemptsSwitch: SwitchPreferenceCompat
    private lateinit var rebootAfterFailedAttemptsAmountPref: EditTextPreference

    private lateinit var customStrongAuthTimeOutSwitch: SwitchPreferenceCompat
    private lateinit var customStrongAuthTimeoutPref: Preference

    private lateinit var workManager: WorkManager

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        this.controlYourDeviceActivity = activity as ControlYourDeviceActivity
        this.workManager = WorkManager.getInstance(controlYourDeviceActivity)

        this.disableBiometricsSwitch =
            findPreference(getString(R.string.pref_disable_fingerprint_now))!!
        this.intervalEnabledSwitch = findPreference(getString(R.string.pref_interval_enabled))!!

        this.disableBiometricsAtTime = findPreference(getString(R.string.pref_disable_start))!!
        this.reEnableBiometricsAtTime = findPreference(getString(R.string.pref_disable_end))!!
        this.applySettingsPref = findPreference(getString(R.string.pref_apply))!!

        this.rebootAfterFailedAttemptsSwitch =
            findPreference(getString(R.string.pref_reboot_after_n_failed_password_attempts_enabled))!!
        this.rebootAfterFailedAttemptsAmountPref =
            findPreference(getString(R.string.pref_reboot_after_n_failed_password_attempts))!!

        this.customStrongAuthTimeOutSwitch =
            findPreference(getString(R.string.pref_custom_strong_auth_timeout_enabled))!!

        this.customStrongAuthTimeoutPref =
            findPreference(getString(R.string.pref_custom_strong_auth_timeout))!!

        this.disableBiometricsSwitch.isChecked =
            controlYourDeviceActivity.isFlagDisabled(DISABLE_BIOMETRICS_FLAG)

        workManager.getWorkInfosByTagLiveData(
            BiometricsDisableWorker.WORKER_TAG
        ).observe(controlYourDeviceActivity) { workInfos ->
            workInfos.forEach { _ ->
                this.disableBiometricsSwitch.isChecked = controlYourDeviceActivity.isFlagDisabled(
                    DISABLE_BIOMETRICS_FLAG
                )
            }
        }

        this.intervalEnabledSwitch.onPreferenceChangeListener =
            IntervalEnabledPreference(
                this.controlYourDeviceActivity,
                this.workManager,
            )

        this.disableBiometricsSwitch.onPreferenceChangeListener = DisableBiometricsPreference(
            this.controlYourDeviceActivity,
        )

        this.applySettingsPref.onPreferenceClickListener = ApplySettingsPreference(
            this.controlYourDeviceActivity, this.workManager,
            this.disableBiometricsAtTime, this.reEnableBiometricsAtTime
        )


        if (this.controlYourDeviceActivity.isDeviceOwner()) {
            this.rebootAfterFailedAttemptsAmountPref.setOnBindEditTextListener { editText ->
                editText.inputType =
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
            }

            this.rebootAfterFailedAttemptsAmountPref.summaryProvider =
                Preference.SummaryProvider<EditTextPreference> { pref ->
                    getString(R.string.number_of_attempts_value, pref.text.toInt())
                }
        } else {
            this.rebootAfterFailedAttemptsSwitch.isEnabled = false
            this.rebootAfterFailedAttemptsSwitch.summary =
                getString(R.string.app_is_not_device_owner)
        }


        val isProfileOwner = this.controlYourDeviceActivity.isProfileOwner() || this.controlYourDeviceActivity.isDeviceOwner()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && isProfileOwner) {
            this.customStrongAuthTimeOutSwitch.onPreferenceChangeListener =
                CustomStrongAuthTimeoutEnabledPreference(this.controlYourDeviceActivity)

            this.customStrongAuthTimeoutPref.onPreferenceClickListener =
                CustomStrongAuthTimeoutPreference(this.controlYourDeviceActivity)
            this.customStrongAuthTimeoutPref.summary = getString(
                R.string.custom_strong_auth_timeout_time_summary,
                this.controlYourDeviceActivity.getStrongAuthTimeout().millisToMinutes()
            )
        } else {
            this.customStrongAuthTimeOutSwitch.isEnabled = false
            this.customStrongAuthTimeOutSwitch.summary = getString(
                if (!isProfileOwner) {
                    R.string.app_is_not_profile_owner
                } else R.string.custom_strong_auth_timeout_not_available
            )
        }
    }

    class DisableBiometricsPreference(
        private val activity: ControlYourDeviceActivity,
    ) : Preference.OnPreferenceChangeListener {
        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
            return try {
                this.activity.updateKeyguardFlag(
                    DISABLE_BIOMETRICS_FLAG,
                    newValue.asBoolean()
                )
                true
            } catch (s: SecurityException) {
                s.printStackTrace()
                false
            }
        }
    }

    class IntervalEnabledPreference(
        val activity: ControlYourDeviceActivity,
        val workManager: WorkManager
    ) : Preference.OnPreferenceChangeListener {
        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
            if (newValue.asBoolean()) {
                workManager.cancelAllWork()
                activity.makeToast(R.string.interval_disabled, Toast.LENGTH_SHORT)
            }

            return true
        }
    }

    class ApplySettingsPreference(
        val activity: ControlYourDeviceActivity,
        val workManager: WorkManager,
        private val disableBiometricsAt: TimepickerPreference,
        private val reEnableBiometricsAt: TimepickerPreference
    ) : Preference.OnPreferenceClickListener {
        override fun onPreferenceClick(preference: Preference?): Boolean {
            val disableAt = disableBiometricsAt.getAsLocalTime()
            val reEnableAt = reEnableBiometricsAt.getAsLocalTime()

            Log.d("Apply", "Disable at: $disableAt")
            Log.d("Apply", "Re-enable at: $reEnableAt")
            workManager.cancelAllWork()

            val now = DateTime.now()

            val disableDt = disableAt.toDateTimeToday()
            val reEnableDt = reEnableAt.toDateTimeToday()

            val disableDtTomorrow = disableDt.plusDays(1)
            val reEnableDtTomorrow = reEnableDt.plusDays(1)

            return buildJobTimes(
                reEnableDt,
                disableDt,
                reEnableDtTomorrow,
                disableDtTomorrow
            ).find {
                it.isValid(now)
            }?.let {
                workManager.enqueue(BiometricsDisableWorker.createJob(it.getRunAt(), it.getState()))
                activity.makeToast(R.string.worker_success, Toast.LENGTH_SHORT)

                true
            } ?: run {
                activity.makeToast(R.string.worker_error, Toast.LENGTH_LONG)
                false
            }
        }


        private fun buildJobTimes(
            reEnableDt: DateTime,
            disableDt: DateTime,
            reEnableDtTomorrow: DateTime,
            disableDtTomorrow: DateTime
        ) = listOf(
            InRangeTime(reEnableDt, disableDt, true),
            InRangeTime(reEnableDt, disableDtTomorrow, true),
            InRangeTime(disableDt, reEnableDt, false),
            InRangeTime(disableDt, reEnableDtTomorrow, false),
            BeforeBothTime(disableDt, reEnableDt),
            AfterBothTime(disableDt, reEnableDt, disableDtTomorrow, reEnableDtTomorrow)
        )
    }

//    class RebootAfterFailedAttemptsPreference(private val activity: ControlYourDeviceActivity) :
//        Preference.OnPreferenceChangeListener {
//        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
//        }
//    }

//    class RebootAfterFailedAttemptsAmountPreference(private val activity: ControlYourDeviceActivity) :
//        Preference.OnPreferenceChangeListener {
//        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
//        }
//    }

    class CustomStrongAuthTimeoutEnabledPreference(private val activity: ControlYourDeviceActivity) :
        Preference.OnPreferenceChangeListener {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
            val profileOwner = activity.isProfileOwner()

            Log.d(
                "Package", "${
                    activity.applicationContext.packageName
                }, profileOwner: $profileOwner"
            )
            /*
                     dpm set-profile-owner fe.timedbiometrics/.ControlDeviceAdminReceiver
                     dpm remove-active-admin fe.timedbiometrics/.ControlDeviceAdminReceiver
                    */
            if (!profileOwner) {
//                AlertDialog.Builder(activity)
//                    .setTitle(R.string.dialog_explain_profile_owner)
//                    .setMessage(R.string.dialog_explain_setup_profile_owner)
//                    .setPositiveButton(R.string.dialog_open) { _, _ ->
//                        //TODO: open browser with url
//                    }
//                    .setNegativeButton(R.string.dialog_cancel, null)
//                    .show()
                return false
            }

            if (!newValue.asBoolean()) {
                activity.setStrongAuthTimeout(0)
            }

            return true
        }
    }

    class CustomStrongAuthTimeoutPreference(
        private val activity: ControlYourDeviceActivity
    ) : Preference.OnPreferenceClickListener {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onPreferenceClick(preference: Preference): Boolean {
            TimeDurationPickerDialog(activity, { _, duration: Long ->
                activity.setStrongAuthTimeout(duration)
                val strongAuthTimeOut = activity.getStrongAuthTimeout()
                preference.summary = if(strongAuthTimeOut != duration) {
                    activity.getString(R.string.custom_strong_auth_timeout_time_summary_mismatch, strongAuthTimeOut.millisToMinutes(), duration.millisToMinutes())
                } else activity.getString(R.string.custom_strong_auth_timeout_time_summary, duration.millisToMinutes())
            }, activity.getStrongAuthTimeout(), TimeDurationPicker.HH_MM).show()

            return true
        }
    }

    override fun onResume() {
        super.onResume()
        this.disableBiometricsSwitch.isChecked =
            controlYourDeviceActivity.isFlagDisabled(DISABLE_BIOMETRICS_FLAG)
    }

    override fun onDisplayPreferenceDialog(preference: Preference?) {
        if (preference is TimepickerPreference) {
            val minutesAfterMidnight = preference.getPersistedMinutesFromMidnight()
            val picker = MaterialTimePicker.Builder()
                .setTimeFormat(requireContext().toTimeFormat())
                .setHour(minutesAfterMidnight / 60)
                .setMinute(minutesAfterMidnight % 60)
                .build()

            picker.addOnPositiveButtonClickListener {
                with((picker.hour * 60) + picker.minute) {
                    preference.persistMinutesFromMidnight(this)
                    preference.summary = preference.minutesFromMidnightToHourlyTime(this)
                }
            }

            picker.show(parentFragmentManager, DIALOG_FRAGMENT_TAG)
        } else {
            super.onDisplayPreferenceDialog(preference)
        }
    }

    companion object {
        private const val DIALOG_FRAGMENT_TAG = "TimePickerDialog"
        private const val DEFAULT_DISABLE_MIN = 1320
        private const val DEFAULT_RE_ENABLE_MIN = 420

        val DISABLE_BIOMETRICS_FLAG = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            DevicePolicyManager.KEYGUARD_DISABLE_BIOMETRICS
        } else DevicePolicyManager.KEYGUARD_DISABLE_FINGERPRINT
    }
}


fun Context.makeToast(text: Int, length: Int) =
    Toast.makeText(this, this.getString(text), length).show()

fun Context.toTimeFormat() =
    if (DateFormat.is24HourFormat(this)) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H

fun Any?.asBoolean() = this?.toString()?.toBooleanStrict() ?: false

fun Long.millisToMinutes() = TimeUnit.MILLISECONDS.toMinutes(this)
