package fe.timedbiometrics.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Warning
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import fe.timedbiometrics.ControlYourDeviceActivity
import fe.timedbiometrics.R
import fe.timedbiometrics.chain
import fe.timedbiometrics.viewmodel.PermissionViewModel

@ExperimentalAnimationApi
@Composable
@Preview
fun PrivilegeWarnCard(
    learnFixClick: () -> Unit = {},
    hideClick: () -> Unit = {},
    viewModel: PermissionViewModel
) {
    val deviceProfileOwner by viewModel.deviceProfileOwner.observeAsState()
    AnimatedVisibility(!deviceProfileOwner!!) {
        Card(
            backgroundColor = Color(0xfffdd835), elevation = 0.dp,
            modifier = Modifier.padding(
                horizontal = 16.dp, vertical = 8.dp
            ), shape = RoundedCornerShape(8.dp)
        ) {
            Column(modifier = Modifier.padding(8.dp)) {
                Row(verticalAlignment = Alignment.Top) {
                    Icon(
                        Icons.Rounded.Warning,
                        modifier = Modifier.size(32.dp),
                        tint = Color.White,
                        contentDescription = stringResource(R.string.accessibility_warning_icon)
                    )

                    Spacer(Modifier.size(4.dp))

                    Column {
                        Text(
                            stringResource(R.string.status_warning),
                            color = Color.White,
                            style = MaterialTheme.typography.h6,
                            fontFamily = ControlYourDeviceActivity.HK_GROTESK_FONT
                        )

                        Text(
                            stringResource(R.string.status_warning_summary),
                            color = Color.White,
                            style = MaterialTheme.typography.subtitle1
                        )
                    }
                }

                Spacer(Modifier.size(16.dp))
                Row {
                    OutlinedButton(
                        colors = ButtonDefaults.outlinedButtonColors(
                            backgroundColor = Color.Transparent, contentColor = Color.White
                        ), border = BorderStroke(1.dp, Color.White), onClick = learnFixClick
                    ) {
                        Text(stringResource(R.string.status_warning_learn_fix), color = Color.White)
                    }

                    Spacer(Modifier.size(6.dp))

                    TextButton(onClick = { viewModel.onDeviceProfileOwnerChange(true) }.chain(hideClick)) {
                        Text(stringResource(R.string.status_warning_hide), color = Color.White)
                    }
                }
            }
        }
    }
}