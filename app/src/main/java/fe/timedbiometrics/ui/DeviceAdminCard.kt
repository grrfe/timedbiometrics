package fe.timedbiometrics.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import fe.timedbiometrics.viewmodel.PermissionViewModel
import fe.timedbiometrics.ControlYourDeviceActivity
import fe.timedbiometrics.R


@ExperimentalAnimationApi
@Composable
fun DeviceAdminCard(promptDeviceAdmin: () -> Unit = {}, viewModel: PermissionViewModel) {
    val active by viewModel.deviceAdminActive.observeAsState()
    AnimatedVisibility(!active!!) {
        Card(
            backgroundColor = Color(0xFFFF0005), elevation = 0.dp,
            modifier = Modifier.padding(
                horizontal = 16.dp, vertical = 8.dp
            ), shape = RoundedCornerShape(8.dp)
        ) {
            Column(modifier = Modifier.padding(8.dp)) {
                Row(verticalAlignment = Alignment.Top) {
                    Icon(
                        painterResource(R.drawable.ic_baseline_error_outline_24),
                        modifier = Modifier.size(32.dp),
                        tint = Color.White,
                        contentDescription = stringResource(R.string.accessibility_warning_icon)
                    )

                    Spacer(Modifier.size(4.dp))

                    Column {
                        Text(
                            stringResource(R.string.device_admin_not_available),
                            color = Color.White,
                            style = MaterialTheme.typography.h6,
                            fontFamily = ControlYourDeviceActivity.HK_GROTESK_FONT
                        )

                        Text(
                            stringResource(R.string.device_admin_not_available_summary),
                            color = Color.White,
                            style = MaterialTheme.typography.subtitle1
                        )
                    }
                }

                Spacer(Modifier.size(16.dp))
                Row {
                    OutlinedButton(
                        colors = ButtonDefaults.outlinedButtonColors(
                            backgroundColor = Color.Transparent, contentColor = Color.White
                        ), border = BorderStroke(1.dp, Color.White), onClick = promptDeviceAdmin
                    ) {
                        Text(
                            stringResource(R.string.device_admin_request_permission),
                            color = Color.White
                        )
                    }
                }
            }
        }
    }
}